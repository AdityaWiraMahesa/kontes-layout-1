import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.indigo[50],
          appBar: AppBar(
            title: Text('Logo Universitas Pendidikan Ganesha'),
            backgroundColor: Colors.blue[400],
          ),
          body: Center(
            child: Image(
              image: NetworkImage(
                  'https://sso.undiksha.ac.id/cas/assets/images/sistem/2207_logo.png'),
            ),
          ))));
}
